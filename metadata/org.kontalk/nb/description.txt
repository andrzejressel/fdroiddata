'''Følg @kontalknet på identi.ca for å holde deg oppdatert med de siste endringene og tjenestetilstand!'''

Kontalk er et sikkert lynmeldingsprogram, som lar deg sende og motta tekst, bilder og lydmeldinger (andre typer kommer snart) til og fra andre Kontalk-brukere helt gratis (*).

Nettverket avhenger av donasjoner fra den tilknyttede gemenskapen for å holde dørene åpne, så hvis du kan, doner på [http://kontalk.net/ kontalk.net]. Selv små bidrag monner.

Du kan finne alle tilganger brukt av programmet forklart på [https://github.com/kontalk/androidclient/wiki/Android-client-permissions denne siden].

Denne versjonen er fri for alle Google-innholdsbestanddeler, som betyr ingen støtte for push-merknader.

(*) Kostnader fra tjenestetilbyder for internettrafikk kan tilkomme

