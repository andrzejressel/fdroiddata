Categories:Internet
License:MPL-2.0
Author Name:Mozilla
Author Email:android-marketplace-notices@mozilla.com
Web Site:https://www.mozilla.org/firefox/focus/
Source Code:https://github.com/mozilla-mobile/focus-android
Issue Tracker:https://github.com/mozilla-mobile/focus-android/issues

Name:Firefox Klar
Auto Name:Firefox Focus

Repo Type:git
Repo:https://github.com/mozilla-mobile/focus-android

Build:1.0,5
    commit=v1.0
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' -e "s/compile(name: 'telemetry-2b0baee', ext: 'aar')/compile 'org.mozilla.telemetry:telemetry:1.0.0'/" build.gradle

Build:1.1,6
    commit=v1.1-RC1
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' build.gradle

Build:1.2.1,9
    commit=v1.2.1
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' build.gradle

Build:1.3,10
    commit=v1.3
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' build.gradle

Build:2.0,11
    commit=v2.0
    subdir=app
    gradle=klar,webkit
    rm=app/libs/*
    prebuild=sed -i -e '/focusCompile/d' -e '/geckoCompile/d' -e '/Dynamically set versionCode/,/^}/d' build.gradle

Maintainer Notes:
Release builds calculate version code dynamically. This is unsuitable for us
because it depends on date and time. Use default version code (supposed to be
used in debug builds): it's a well-formed static constant bumped every release.
.

Auto Update Mode:Version v%v
Update Check Mode:Tags ^v[0-9.]+$
Update Check Name:org.mozilla
Current Version:2.0
Current Version Code:11
